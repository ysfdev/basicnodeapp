const mongoose = require('mongoose');

//setup db connection
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost/test', {
    useMongoClient: true
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Mongoose succesfully connected to db')
});

module.exports.dbConnection = db;