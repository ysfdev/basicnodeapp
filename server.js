const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

//Setup dbConnection
require('./config/database').dbConnection;

//Set view engine 
app.set('view engine', 'pug')

//Connect routes to app
require('./routes')(app);

//Error handling
app.use(function (error, request, response, next) {
 console.error(error.stack);
 response.status(400).send(error.message);
});

// Start server
app.listen(port, function() {
 console.log(`Node app is running at http://localhost:${port}`);
});