'use strict';
const Meetup = require('../models/Meetup');

//Load default meetup
module.exports = (req, res) => {
    let hostName = req.params.hostName;
    Meetup.findOne({"host" : hostName}).then((meetup) => {
        res.render('index', {
            title: "Welcome",
            meetupInfo: meetup.info()
        });
    })
    .catch((err) => {
        console.error(err);
        res.send('Unable to load meetup');
    })
};

//Create meetup index
module.exports.generateMeetup = (req, res) => {
    //Test data
    let meetupData = { 
        name: "Building and Sctructuring NodeJs Apps",
        host: "yeramin",
        date: new Date(),
    };

    let newMeetup = new Meetup(meetupData);

    newMeetup.save(meetupData)
    .then((newMeetupData) => {
        res.send('updated');
    })
    .catch((err) => {
        res.send('Unable to save meetup');
    })
};
