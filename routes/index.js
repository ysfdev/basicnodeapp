'use strict';
const meetupController = require('../controllers/meetup-controller');

module.exports = (app) => {
    app.get('/meetups/:hostName', meetupController);
    app.get('/generatetestmeetup', meetupController.generateMeetup);
};