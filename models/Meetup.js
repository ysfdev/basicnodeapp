const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MeetupSchema = new Schema({
    name: { 
        type: String,
        required: [true, "Meetup name is required"]
    },
    host: { 
        type: String,
        required: [true, "Meetup host is required"]
    },
    date: { 
        type: Date,
        required: [true, "Meetup date is required"],
        default: new Date()
    },
    maxGuests: { 
        type: Number,
        default: 100 
    }
});

//SCHEMA METHODS
MeetupSchema.methods.info = function() {
    let info = (this.name && this.host && this.date) 
    ? `${this.name} is being presented by ${this.host} on ${this.date.toLocaleDateString()}`
    : "No meetup info found";
    return info;
};

module.exports = mongoose.model('Meetup', MeetupSchema);